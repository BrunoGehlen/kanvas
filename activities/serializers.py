from rest_framework import serializers


class ActivitySerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    user_id = serializers.PrimaryKeyRelatedField(read_only=True)
    repo = serializers.CharField(read_only=True)
    grade = serializers.IntegerField(read_only=True)
