from rest_framework.permissions import BasePermission


class ActivityPermissions(BasePermission):
    def has_permission(self, request, view):
        return request.user.is_superuser == True


# from rest_framework.permissions import SAFE_METHODS
# class FacilitatorEditProtected(permissions.BasePermission):
#     def has_permission(self, request, view):
#         if request.method in SAFE_METHODS:
#             return True
#         return request.user and request.user.is_staff


# SAFE_METHODS = ('GET', 'HEAD', 'OPTIONS')