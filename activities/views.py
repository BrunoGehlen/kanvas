from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from .permission import ActivityPermissions
from .serializers import ActivitySerializer
from django.contrib.auth.models import User
from rest_framework.views import APIView
from django.shortcuts import render
from rest_framework import status
from .models import Activity


class ActivitiesView(APIView):
    queryset = Activity.objects.none()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, student_id=""):
        if request.user.is_staff or request.user.is_superuser:

            if student_id:
                activities = Activity.objects.filter(user_id=student_id).all()

            else:
                activities = Activity.objects.all()

            serializer = ActivitySerializer(activities, many=True)

            return Response(serializer.data, status=status.HTTP_200_OK)

        else:

            activities = Activity.objects.filter(user_id=request.user.id).all()
            serializer = ActivitySerializer(activities, many=True)

            return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = ActivitySerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)

        existing_repo = Activity.objects.filter(repo=request.data["repo"]).all()

        if existing_repo:
            return Response(serializer.data, status=status.HTTP_200_OK)

        if request.user.is_staff or request.user.is_superuser:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        request.data["grade"] = None

        activity = Activity.objects.create(
            repo=request.data["repo"], user_id=request.user
        )

        serializer = ActivitySerializer(activity)

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def put(self, request):

        if not request.user.is_staff:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        activity = Activity.objects.get(pk=request.data['id'])
        activity.grade = request.data['grade']
        serializer = ActivitySerializer(activity)
        activity.save()
        
        return Response(serializer.data, status=status.HTTP_201_CREATED)
