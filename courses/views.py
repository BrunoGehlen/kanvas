from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from .models import Course
from .serializers import CourseSerializer
from .permissions import CoursePermissions

from django.contrib.auth import authenticate
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.authentication import TokenAuthentication
from django.core.exceptions import ObjectDoesNotExist

# from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User


class CourseView(APIView):
    queryset = Course.objects.none()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly, CoursePermissions]

    def get(self, request):
        courses = Course.objects.all()
        serializer = CourseSerializer(courses, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = CourseSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        data = serializer.data
        if data.get('user_set'):
            user_set = data.pop("user_set")

            for user in user_set:
                Course.objects.add(user)

        course = Course.objects.create(**data)
        serializer = CourseSerializer(course)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request):
        data = request.data
        try:
            course = Course.objects.get(id=data["course_id"])
            users = course.user_set.all()
            for user in users:
                course.user_set.remove(user)

            for user in data["user_ids"]:
                add_user = User.objects.get(id=user)
                course.user_set.add(add_user)

            serializer = CourseSerializer(course)

            return Response(serializer.data, status=status.HTTP_200_OK)

        except ObjectDoesNotExist:
            return Response({'mensagem':'courses invalid or user_ids'},status=status.HTTP_404_NOT_FOUND)