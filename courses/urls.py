from django.urls import path
from .views import CourseView

urlpatterns = [
    path('courses/', CourseView.as_view()),
    path('courses/registrations/', CourseView.as_view())
]